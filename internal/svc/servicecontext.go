package svc

import (
	"github.com/myhz79/arvan-challenge/internal/config"
	"github.com/myhz79/arvan-challenge/internal/middleware"
	ddp "github.com/myhz79/arvan-challenge/internal/pkg/deduplicate"
	"github.com/zeromicro/go-zero/core/stores/redis"
	"github.com/zeromicro/go-zero/rest"
)

type ServiceContext struct {
	Config       config.Config
	Redis        *redis.Redis
	Ratelimit    rest.Middleware
	Quota        rest.Middleware
	Deduplicator ddp.MessageDeDuplicate
}

func NewServiceContext(c config.Config) *ServiceContext {

	redis := redis.MustNewRedis(c.Redis)

	return &ServiceContext{
		Config:       c,
		Redis:        redis,
		Ratelimit:    middleware.NewRatelimitMiddleware(redis, c.Ratelimit.Period).Handle,
		Quota:        middleware.NewQuotaMiddleware(redis).Handle,
		Deduplicator: ddp.NewMessageDeDuplicate(redis, c.Deduplicate.Expire),
	}
}
