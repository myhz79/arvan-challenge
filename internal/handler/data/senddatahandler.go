package data

import (
	"encoding/json"
	"net/http"

	"github.com/myhz79/arvan-challenge/internal/logic/data"
	"github.com/myhz79/arvan-challenge/internal/middleware"
	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/rest/httpx"
)

// Send Data
func SendDataHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.SendDataRequest
		if err := httpx.Parse(r, &req); err != nil {
			logx.WithContext(r.Context()).Debugf("r body: %+v", r)
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		// Unmarshall
		var reqData middleware.ReqData
		if err := middleware.ValidateRequest(r, &reqData); err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		// check if request is duplicate
		isDuplicate, err := svcCtx.Deduplicator.HasDuplicateMessage(r.Context(), reqData.UserId, reqData.RequestId)
		// ignore error
		_ = err

		var cacheResponse types.SendDataResponse
		if isDuplicate {
			// retriev cached response
			cacheResp, err := svcCtx.Deduplicator.GetDuplicateMessage(r.Context(), reqData.UserId, reqData.RequestId)
			// ignore error
			_ = err
			err = json.Unmarshal([]byte(cacheResp), &cacheResponse)
			if err != nil {
				httpx.ErrorCtx(r.Context(), w, err)
				return
			}

			logx.WithContext(r.Context()).Debugf("sending cached response: %+v", cacheResponse)
			httpx.OkJsonCtx(r.Context(), w, cacheResponse)
			return

		}

		l := data.NewSendDataLogic(r.Context(), svcCtx)
		resp, err := l.SendData(&req)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
		} else {
			// cache response on success
			svcCtx.Deduplicator.PutDuplicateMessage(r.Context(), reqData.UserId, reqData.RequestId, resp)
			httpx.OkJsonCtx(r.Context(), w, resp)
		}
	}
}
