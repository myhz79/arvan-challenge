package data

import (
	"net/http"

	"github.com/myhz79/arvan-challenge/internal/logic/data"
	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"
	"github.com/zeromicro/go-zero/rest/httpx"
)

// Delete Data
func DeleteDataHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteDataRequest
		if err := httpx.Parse(r, &req); err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		l := data.NewDeleteDataLogic(r.Context(), svcCtx)
		err := l.DeleteData(&req)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
		} else {
			httpx.Ok(w)
		}
	}
}
