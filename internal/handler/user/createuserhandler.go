package user

import (
	"net/http"

	"github.com/myhz79/arvan-challenge/internal/logic/user"
	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"
	"github.com/zeromicro/go-zero/rest/httpx"
)

// Create User
func CreateUserHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.CreateUserRequest
		if err := httpx.Parse(r, &req); err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		l := user.NewCreateUserLogic(r.Context(), svcCtx)
		resp, err := l.CreateUser(&req)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
		} else {
			httpx.OkJsonCtx(r.Context(), w, resp)
		}
	}
}
