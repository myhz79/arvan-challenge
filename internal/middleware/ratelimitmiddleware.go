package middleware

import (
	"net/http"
	"strconv"

	"github.com/myhz79/arvan-challenge/internal/pkg/errors"
	"github.com/zeromicro/go-zero/core/limit"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/redis"
	"github.com/zeromicro/go-zero/rest/httpx"
)

type RatelimitMiddleware struct {
	Redis  *redis.Redis
	Period int
}

func NewRatelimitMiddleware(redis *redis.Redis, period int) *RatelimitMiddleware {
	return &RatelimitMiddleware{
		Redis:  redis,
		Period: period,
	}
}

func (m *RatelimitMiddleware) Handle(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var reqData ReqData
		err := ValidateRequest(r, &reqData)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		rateLimitStr, err := m.Redis.GetCtx(r.Context(), MakeUserRateLimitKey(reqData.UserId))
		if err != nil {
			logx.WithContext(r.Context()).Error(err)
			// ignore error
			next(w, r)
			return
		}

		rateLimitInt, err := strconv.Atoi(rateLimitStr)
		if err != nil {
			logx.WithContext(r.Context()).Errorf("failed to convert rate limit string to int: %v", err)
			next(w, r)
			return
		}

		limiter := limit.NewPeriodLimit(m.Period, rateLimitInt, m.Redis, rateLimitPrefix)

		permit, err := limiter.TakeCtx(r.Context(), reqData.UserId)
		if err != nil {
			logx.WithContext(r.Context()).Debugf("error limiting request: %v", err)
			next(w, r)
			return
		}

		if permit == limit.OverQuota {
			logx.WithContext(r.Context()).Debugf("ratelimit reached for user(%v)", reqData.UserId)
			httpx.ErrorCtx(r.Context(), w, errors.TooManyRequests)
			return
		}

		logx.WithContext(r.Context()).Debugf("user(%v) is permited with rate(%v)", reqData.UserId, permit)
		next(w, r)

	}
}
