package middleware

import (
	"fmt"
	"net/http"
	"time"

	"github.com/myhz79/arvan-challenge/internal/pkg/errors"
	"github.com/zeromicro/go-zero/core/logx"
)

const rateLimitPrefix = "ratelimit#"
const trafficLimitPrefix = "trafficlimit#"

const userRateLimitPrefix = "user_ratelimit"
const userTrafficLimitPrefix = "user_trafficlimit"

type ReqData struct {
	RequestId string
	UserId    string
	Password  string
}

func getRequestID(r *http.Request) (reqId string) {
	reqId = r.Header.Get("X-Request-Id")
	return

}

func getUserAndPass(r *http.Request) (user, pass string) {
	user, pass, _ = r.BasicAuth()
	return
}

func getFirstOfNextMonth() int {
	// Get current time
	now := time.Now()

	// Get first day of next month
	nextMonth := now.AddDate(0, 1, 0)
	firstDayOfNextMonth := time.Date(nextMonth.Year(), nextMonth.Month(), 1, 0, 0, 0, 0, nextMonth.Location())

	// Convert to Unix timestamp
	return int(firstDayOfNextMonth.Unix())
}

func ValidateRequest(r *http.Request, data *ReqData) error {
	// extract requestId
	data.RequestId = getRequestID(r)
	if data.RequestId == "" {
		logx.WithContext(r.Context()).Debugf("empty requestid for req(%+v)", r)
		return errors.BadRequest
	}

	// extract authenication details
	userId, pass, ok := r.BasicAuth()
	_ = pass

	data.UserId = userId
	data.Password = pass

	// validate request
	if !ok {
		logx.WithContext(r.Context()).Debugf("erro parsing auth header in req(%+v)", r)
		return errors.BadRequest
	}

	if userId == "" {
		logx.WithContext(r.Context()).Debugf("empty userid for req(%+v)", r)
		return errors.BadRequest
	}

	return nil

}

func MakeUserRateLimitKey(userId string) (key string) {
	return fmt.Sprintf("%s#%s", userRateLimitPrefix, userId)
}

func MakeUserTrafficLimitKey(userId string) (key string) {
	return fmt.Sprintf("%s#%s", userTrafficLimitPrefix, userId)
}
