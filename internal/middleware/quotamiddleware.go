package middleware

import (
	"net/http"
	"strconv"

	"github.com/myhz79/arvan-challenge/internal/pkg/errors"
	"github.com/myhz79/arvan-challenge/internal/pkg/trafficlimit"
	"github.com/zeromicro/go-zero/core/limit"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/redis"
	"github.com/zeromicro/go-zero/rest/httpx"
)

type QuotaMiddleware struct {
	Redis *redis.Redis
}

func NewQuotaMiddleware(redis *redis.Redis) *QuotaMiddleware {
	return &QuotaMiddleware{
		Redis: redis,
	}
}

func (m *QuotaMiddleware) Handle(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		var reqData ReqData
		err := ValidateRequest(r, &reqData)
		if err != nil {
			httpx.ErrorCtx(r.Context(), w, err)
			return
		}

		trafficLimitStr, err := m.Redis.GetCtx(r.Context(), MakeUserTrafficLimitKey(reqData.UserId))
		if err != nil {
			logx.WithContext(r.Context()).Error(err)
			// ignore error
			next(w, r)
			return
		}

		trafficLimitInt, err := strconv.Atoi(trafficLimitStr)
		if err != nil {
			logx.WithContext(r.Context()).Errorf("failed to convert traffic limit string to int: %v", err)
			next(w, r)
			return
		}

		limiter := trafficlimit.NewTrafficLimit(getFirstOfNextMonth(), trafficLimitInt, m.Redis, trafficLimitPrefix)

		permit, err := limiter.TakeCtx(r.Context(), reqData.UserId, r.ContentLength)

		if err != nil {
			logx.WithContext(r.Context()).Debugf("error limiting request traffic: %v", err)
			next(w, r)
		}

		if permit == limit.OverQuota {
			logx.WithContext(r.Context()).Debugf("trafficlimit reached for user(%v)", reqData.UserId)
			httpx.ErrorCtx(r.Context(), w, errors.TrafficLimitReached)
			return
		}

		next(w, r)
	}
}
