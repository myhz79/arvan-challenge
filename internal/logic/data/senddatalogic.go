package data

import (
	"context"

	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type SendDataLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

// Send Data
func NewSendDataLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SendDataLogic {
	return &SendDataLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *SendDataLogic) SendData(req *types.SendDataRequest) (resp *types.SendDataResponse, err error) {

	resp = &types.SendDataResponse{}

	// some logic here
	resp.Data = req.Data
	// some logic here

	return
}
