package data

import (
	"context"

	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeleteDataLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

// Delete Data
func NewDeleteDataLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteDataLogic {
	return &DeleteDataLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *DeleteDataLogic) DeleteData(req *types.DeleteDataRequest) error {
	// todo: add your logic here and delete this line

	return nil
}
