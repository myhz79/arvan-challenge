package user

import (
	"context"
	"strconv"

	"github.com/myhz79/arvan-challenge/internal/middleware"
	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type CreateUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

// Create User
func NewCreateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateUserLogic {
	return &CreateUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *CreateUserLogic) CreateUser(req *types.CreateUserRequest) (resp *types.CreateUserResponse, err error) {

	l.svcCtx.Redis.SetCtx(l.ctx, middleware.MakeUserRateLimitKey(req.UserId), strconv.Itoa(int(req.RateLimit)))
	l.svcCtx.Redis.SetCtx(l.ctx, middleware.MakeUserTrafficLimitKey(req.UserId), strconv.Itoa(int(req.MonthlyTrafficLimit)))

	//todo@myhz79: check errors

	return &types.CreateUserResponse{
		User: types.User{
			UserId:              req.UserId,
			RateLimit:           req.RateLimit,
			MonthlyTrafficLimit: req.MonthlyTrafficLimit,
		},
	}, nil
}
