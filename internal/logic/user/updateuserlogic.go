package user

import (
	"context"

	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateUserLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

// Update User
func NewUpdateUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateUserLogic {
	return &UpdateUserLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *UpdateUserLogic) UpdateUser(req *types.CreateUserRequest) (resp *types.CreateUserResponse, err error) {
	// todo: add your logic here and delete this line

	return
}
