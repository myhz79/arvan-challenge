package trafficlimit

import (
	"context"
	"errors"
	"strconv"

	"github.com/zeromicro/go-zero/core/stores/redis"
)

const (
	// Unknown means not initialized state.
	Unknown = iota
	// Allowed means allowed state.
	Allowed
	// HitQuota means this request exactly hit the quota.
	HitQuota
	// OverQuota means passed the quota.
	OverQuota

	internalOverQuota = 0
	internalAllowed   = 1
	internalHitQuota  = 2
)

var (
	// ErrUnknownCode is an error that represents unknown status code.
	ErrUnknownCode = errors.New("unknown status code")

	// to be compatible with aliyun redis, we cannot use `local key = KEYS[1]` to reuse the key
	periodScript = redis.NewScript(`local limit = tonumber(ARGV[1])
local deadline = tonumber(ARGV[2])
local size = tonumber(ARGV[3])
local current = redis.call("INCRBY", KEYS[1], size)
if current == size then
    redis.call("expireat", KEYS[1], deadline)
end
if current < limit then
    return 1
elseif current == limit then
    return 2
else
    return 0
end`)
)

type (
	// TrafficOption defines the method to customize a TrafficLimit.
	TrafficOption func(l *TrafficLimit)

	// A TrafficLimit is used to limit requests during a period of time.
	TrafficLimit struct {
		expiry     int
		quota      int
		limitStore *redis.Redis
		keyPrefix  string
		align      bool
	}
)

// NewTrafficLimit returns a TrafficLimit with given parameters.
func NewTrafficLimit(expiry, quota int, limitStore *redis.Redis, keyPrefix string,
	opts ...TrafficOption) *TrafficLimit {
	limiter := &TrafficLimit{
		expiry:     expiry,
		quota:      quota,
		limitStore: limitStore,
		keyPrefix:  keyPrefix,
	}

	for _, opt := range opts {
		opt(limiter)
	}

	return limiter
}

// Take requests a permit, it returns the permit state.
func (h *TrafficLimit) Take(key string, trafficSize int64) (int, error) {
	return h.TakeCtx(context.Background(), key, trafficSize)
}

// TakeCtx requests a permit with context, it returns the permit state.
func (h *TrafficLimit) TakeCtx(ctx context.Context, key string, trafficSize int64) (int, error) {
	resp, err := h.limitStore.ScriptRunCtx(ctx, periodScript, []string{h.keyPrefix + key}, []string{
		strconv.Itoa(h.quota),
		strconv.Itoa(h.calcExpireAtSeconds()),
		strconv.Itoa(int(trafficSize)),
	})
	if err != nil {
		return Unknown, err
	}

	code, ok := resp.(int64)
	if !ok {
		return Unknown, ErrUnknownCode
	}

	switch code {
	case internalOverQuota:
		return OverQuota, nil
	case internalAllowed:
		return Allowed, nil
	case internalHitQuota:
		return HitQuota, nil
	default:
		return Unknown, ErrUnknownCode
	}
}

func (h *TrafficLimit) calcExpireAtSeconds() int {
	// if h.align {
	// 	now := time.Now()
	// 	_, offset := now.Zone()
	// 	unix := now.Unix() + int64(offset)
	// 	return h.period - int(unix%int64(h.period))
	// }

	return h.expiry
}

// Align returns a func to customize a TrafficLimit with alignment.
// For example, if we want to limit end users with 5 sms verification messages every day,
// we need to align with the local timezone and the start of the day.
func Align() TrafficOption {
	return func(l *TrafficLimit) {
		l.align = true
	}
}
