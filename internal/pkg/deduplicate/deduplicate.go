package deduplicate

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/redis"
)

const (
	duplicateMessageData = "duplicate_message_data"
	// expireTimeout        = 60 // 60s
)

func makeDuplicateMessageKey(prefix string, senderUserId, clientRandomId string) string {
	return fmt.Sprintf("%s_%s_%s", prefix, senderUserId, clientRandomId)
}

type MessageDeDuplicate struct {
	kv            *redis.Redis
	expireTimeout int
}

func NewMessageDeDuplicate(redis *redis.Redis, expireAfter int) MessageDeDuplicate {
	return MessageDeDuplicate{
		kv:            redis,
		expireTimeout: expireAfter,
	}
}

func (m *MessageDeDuplicate) HasDuplicateMessage(ctx context.Context, senderUserId, deDuplicateId string) (bool, error) {
	k := makeDuplicateMessageKey(duplicateMessageData, senderUserId, deDuplicateId)

	isAvailable, err := m.kv.ExistsCtx(ctx, k)
	if err != nil {
		logx.WithContext(ctx).Errorf("checkDuplicateMessage - INCR {%s}, error: {%v}", k, err)
		return false, err
	}

	return isAvailable, nil
}

func (m *MessageDeDuplicate) PutDuplicateMessage(ctx context.Context, senderUserId, deDuplicateId string, response any) error {
	k := makeDuplicateMessageKey(duplicateMessageData, senderUserId, deDuplicateId)
	cacheData, _ := json.Marshal(response)

	if err := m.kv.SetexCtx(ctx, k, string(cacheData), m.expireTimeout); err != nil {
		logx.WithContext(ctx).Errorf("putDuplicateMessage - SET {%s, %s, %d}, error: %s", k, cacheData, m.expireTimeout, err)
		return err
	}

	return nil
}

func (m *MessageDeDuplicate) GetDuplicateMessage(ctx context.Context, senderUserId, clientRandomId string) (string, error) {
	k := makeDuplicateMessageKey(duplicateMessageData, senderUserId, clientRandomId)

	cacheData, err := m.kv.GetCtx(ctx, k)
	if err != nil {
		logx.WithContext(ctx).Errorf("getDuplicateMessage - GET {%s}, error: %s", k, err)
		return "", err
	}

	return cacheData, nil
}
