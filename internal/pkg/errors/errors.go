package errors

import (
	"net/http"

	"github.com/zeromicro/x/errors"
)

// app
var CodeAlreadySent = errors.New(http.StatusBadRequest, "CODE_ALREADY_SENT")

var InvalidCredentials = errors.New(http.StatusBadRequest, "INVALID_CREDENTIALS")

var TokenExpired = errors.New(http.StatusUnauthorized, "TOKEN_EXPIRED")

var InvalidToken = errors.New(http.StatusUnauthorized, "INVALID_TOKEN")

var InvalidPhoneNumber = errors.New(http.StatusBadRequest, "INVALID_PHONE_NUMBER")

var UserNotFound = errors.New(http.StatusNotFound, "USER_NOT_FOUND")

var UserBanned = errors.New(http.StatusNotFound, "USER_BANNED")

var InvalidFirstname = errors.New(http.StatusBadRequest, "INVALID_FIRSTNAME")

var InvalidLastname = errors.New(http.StatusBadRequest, "INVALID_LASTNAME")

var InvalidEmail = errors.New(http.StatusBadRequest, "INVALID_EMAIL")

var EmailProviderNotSupported = errors.New(http.StatusBadRequest, "EMAIL_PROVIDER_NOT_SUPPORTED")

var GoalNotFound = errors.New(http.StatusNotFound, "GOAL_NOT_FOUND")

var TooManyRequests = errors.New(http.StatusTooManyRequests, "TOO_MANY_REQUESTS")

var TrafficLimitReached = errors.New(http.StatusTooManyRequests, "TRAFFIC_LIMIT_REACHED")

// 400
// StatusBadRequest represents a 400 Bad Request error.
var BadRequest = errors.New(http.StatusBadRequest, "BAD_REQUEST")

// StatusUnauthorized represents a 401 Unauthorized error.
var Unauthorized = errors.New(http.StatusUnauthorized, "UNAUTHORIZED")

// StatusPaymentRequired represents a 402 Payment Required error.
var PaymentRequired = errors.New(http.StatusPaymentRequired, "PAYMENT_REQUIRED")

// StatusForbidden represents a 403 Forbidden error.
var Forbidden = errors.New(http.StatusForbidden, "FORBIDDEN")

// StatusNotFound represents a 404 Not Found error.
var NotFound = errors.New(http.StatusNotFound, "NOT_FOUND")

// StatusMethodNotAllowed represents a 405 Method Not Allowed error.
var MethodNotAllowed = errors.New(http.StatusMethodNotAllowed, "METHOD_NOT_ALLOWED")

// StatusNotAcceptable represents a 406 Not Acceptable error.
var NotAcceptable = errors.New(http.StatusNotAcceptable, "NOT_ACCEPTABLE")

// StatusProxyAuthRequired represents a 407 Proxy Authentication Required error.
var ProxyAuthRequired = errors.New(http.StatusProxyAuthRequired, "PROXY_AUTHENTICATION_REQUIRED")

// StatusRequestTimeout represents a 408 Request Timeout error.
var RequestTimeout = errors.New(http.StatusRequestTimeout, "REQUEST_TIMEOUT")

// StatusConflict represents a 409 Conflict error.
var Conflict = errors.New(http.StatusConflict, "CONFLICT")

// StatusGone represents a 410 Gone error.
var Gone = errors.New(http.StatusGone, "GONE")

// StatusLengthRequired represents a 411 Length Required error.
var LengthRequired = errors.New(http.StatusLengthRequired, "LENGTH_REQUIRED")

// StatusPreconditionFailed represents a 412 Precondition Failed error.
var PreconditionFailed = errors.New(http.StatusPreconditionFailed, "PRECONDITION_FAILED")

// StatusRequestEntityTooLarge represents a 413 Request Entity Too Large error.
var RequestEntityTooLarge = errors.New(http.StatusRequestEntityTooLarge, "REQUEST_ENTITY_TOO_LARGE")

// StatusRequestURITooLong represents a 414 Request-URI Too Long error.
var RequestURITooLong = errors.New(http.StatusRequestURITooLong, "REQUEST_URI_TOO_LONG")

// StatusUnsupportedMediaType represents a 415 Unsupported Media Type error.
var UnsupportedMediaType = errors.New(http.StatusUnsupportedMediaType, "UNSUPPORTED_MEDIA_TYPE")

// StatusRequestedRangeNotSatisfiable represents a 416 Requested Range Not Satisfiable error.
var RequestedRangeNotSatisfiable = errors.New(http.StatusRequestedRangeNotSatisfiable, "REQUESTED_RANGE_NOT_SATISFIABLE")

// StatusExpectationFailed represents a 417 Expectation Failed error.
var ExpectationFailed = errors.New(http.StatusExpectationFailed, "EXPECTATION_FAILED")

// StatusTeapot represents a 418 I'm a Teapot error.
var Teapot = errors.New(http.StatusTeapot, "I'M_A_TEAPOT")

// 500
// StatusInternalServerError represents an internal server error
var InternalServerError = errors.New(http.StatusInternalServerError, "INTERNAL_SERVER_ERROR")

// StatusNotImplemented represents an HTTP 501 Not Implemented error
var NotImplemented = errors.New(http.StatusNotImplemented, "NOT_IMPLEMENTED")

// StatusBadGateway represents an HTTP 502 Bad Gateway error
var BadGateway = errors.New(http.StatusBadGateway, "BAD_GATEWAY")

// StatusServiceUnavailable represents an HTTP 503 Service Unavailable error
var ServiceUnavailable = errors.New(http.StatusServiceUnavailable, "SERVICE_UNAVAILABLE")

// StatusGatewayTimeout represents an HTTP 504 Gateway Timeout error
var GatewayTimeout = errors.New(http.StatusGatewayTimeout, "GATEWAY_TIMEOUT")
