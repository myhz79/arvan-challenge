package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"

	"github.com/myhz79/arvan-challenge/internal/config"
	"github.com/myhz79/arvan-challenge/internal/handler"
	"github.com/myhz79/arvan-challenge/internal/svc"
	"github.com/myhz79/arvan-challenge/internal/types"

	"github.com/zeromicro/go-zero/core/conf"
	"github.com/zeromicro/go-zero/rest"
	"github.com/zeromicro/go-zero/rest/httpx"
	"github.com/zeromicro/x/errors"
)

var configFile = flag.String("f", "etc/arvanchallenge-api.yaml", "the config file")

func main() {
	flag.Parse()

	var c config.Config
	conf.MustLoad(*configFile, &c)

	server := rest.MustNewServer(c.RestConf)
	defer server.Stop()

	ctx := svc.NewServiceContext(c)
	handler.RegisterHandlers(server, ctx)

	httpx.SetOkHandler(func(ctx context.Context, a any) any {
		return &types.GeneralResp{
			OK:   true,
			Data: a,
		}
	})

	httpx.SetErrorHandler(func(err error) (int, any) {
		switch e := err.(type) {
		case *errors.CodeMsg:
			return e.Code, &types.GeneralResp{
				OK:    false,
				Error: e.Msg,
			}
		default:
			return http.StatusInternalServerError, &types.GeneralResp{
				OK: false,
				// Error: http.StatusText(http.StatusInternalServerError),
				Error: err.Error(), // only for development
			}
		}
	})

	httpx.SetErrorHandler(func(err error) (int, any) {
		switch e := err.(type) {
		case *errors.CodeMsg:
			return e.Code, &types.GeneralResp{
				OK:    false,
				Error: e.Msg,
			}
		default:
			return http.StatusInternalServerError, &types.GeneralResp{
				OK: false,
				// Error: http.StatusText(http.StatusInternalServerError),
				Error: err.Error(), // only for development
			}
		}
	})

	fmt.Printf("Starting server at %s:%d...\n", c.Host, c.Port)
	server.Start()
}
