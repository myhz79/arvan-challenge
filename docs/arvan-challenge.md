# arvan challenge
challenge project of arvancloud interview.

## Sending Request ID and Auth Data
the 'X-Request-Id' Must contain request unique ID

Authorization header must contain userId [and maybe pass] (Basic Auth)

## General Response Format
all responses will follow this format:

```golang
type GeneralResp struct{
	OK    bool        `json:"ok"`
	Data  interface{} `json:"data,omitempty"`
	Error string      `json:"error,omitempty"`
}
```
- 'ok' parameter will determine response state
- 'data' parameter will carry response payload if request succeeded
- 'error' parameter will carry error message if request failed


## Methods
### 1. Send Data

1. route definition

- Url: /data
- Method: POST
- Request: `SendDataRequest`
- Response: `SendDataResponse`

2. request definition



```golang
type SendDataRequest struct {
	Data string `json:"data,omitempty"`
}
```


3. response definition



```golang
type SendDataResponse struct {
	Data string `json:"data,omitempty"`
}
```

### 2. Delete Data

1. route definition

- Url: /data
- Method: DELETE
- Request: `DeleteDataRequest`
- Response: `-`

2. request definition



```golang
type DeleteDataRequest struct {
	UUID string `json:"uuid"` // identifier of request
}
```


3. response definition


### 3. Create User

1. route definition

- Url: /user/data
- Method: POST
- Request: `CreateUserRequest`
- Response: `CreateUserResponse`

2. request definition



```golang
type CreateUserRequest struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}

type User struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyTraffic int64 `json:"monthly_quota,omitempty"`
}
```


3. response definition



```golang
type CreateUserResponse struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}

type User struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}
```

### 4. Update User

1. route definition

- Url: /user/data
- Method: PUT
- Request: `CreateUserRequest`
- Response: `CreateUserResponse`

2. request definition



```golang
type CreateUserRequest struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}

type User struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}
```


3. response definition



```golang
type CreateUserResponse struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}

type User struct {
	UserId string `json:"user_id,omitempty"`
	RateLimit int64 `json:"rate_limit,omitempty"`
	MonthlyQuota int64 `json:"monthly_quota,omitempty"`
}
```

