## Metrics

an endpoint is defined at "/metrics" path of 6060 port which exposes prometheus related metrics. metrics contain counters and gauges to track requests, response time, errors etc.