FROM golang:1.21 AS builder
WORKDIR /app
COPY . .
ENV TZ="Asia/Tehran"
RUN go env -w GOPROXY=https://goproxy.cn,direct
RUN go build

FROM ubuntu:latest
WORKDIR /app
RUN ls -hal
COPY  ./etc ./etc
COPY --from=builder /app/arvan-challenge .
ENV TZ="Asia/Tehran"
RUN  chmod +x /app/arvan-challenge
CMD ["./arvan-challenge"]
